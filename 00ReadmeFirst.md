# 0. Included codes

* 00ReadmeFirst
* requirements.txt
* constraints.txt
* openslide_py.patch
* openSlidePythonGUI.py
* tests/{boxes.tiff,small.svs}

# 1. Preparing environment

## 1.1. install required third-party C-libraries

  - [openslide C-library|http://www.openslide.org/]
  - Tcl-Tk
  - Python

## 1.2. install required python-modules

  - required modules are listed in "requirements.txt"

    ```
	$ pip install -r requirements.txt
	```
	
	It install required modules and some dependent modules. 

  - if you prepare same-environment and the versions with us. Please refer "constraints.txt" as
  
    ```
	$ pip install -r requirements.txt -c constraints.txt
	```
	
## 1.3. patch pre-installed openslide-python module

  ```
  $ patch -d lib/python3.7/site-packages/openslide < openslide_py.patch
  ```
  
  - sorry. We have to patch although don't want to touch original module. We are now developping alternative module.

# 2. Run

  ```
  $ python openSlidePythonGUI.py
  ```

# 3. Sample images

http://openslide.cs.cmu.edu/download/openslide-testdata/

